import "reflect-metadata";
import { importSchema } from "graphql-import";
import { GraphQLServer } from "graphql-yoga";
import { createConnection } from "typeorm";
import * as path from "path";

import { resolvers } from "./resolvers";

const typeDefs = importSchema(path.join(__dirname, "./schema.graphql"));

const opts = {
  port: 4000,
  cors: {
    credentials: true,
    origin: ["http://localhost:3500"] 
  }
};

const server = new GraphQLServer({ typeDefs, resolvers });

createConnection().then(() => {
  server.start(opts, () => console.log("Server is running on localhost:4000"));
});
