import * as bcrypt from 'bcryptjs';
import * as yup from "yup";
import {GQL} from './types/schema'
import { User } from './entity/User';
import { Post } from './entity/Post';
import {ValidationError} from "yup";

const schemaUserAccount = yup.object().shape({
    name: yup.string().min(5).max(30),
    email: yup.string().min(1).max(255).email(),
    password: yup.string().min(6).max(255)
});

const schemaCreatePost = yup.object().shape({
    owner: yup.string().required(),
    description: yup.string().min(1).max(255),
    content: yup.string().min(10).max(255)
});


const formatErrors = (err : ValidationError) => {
    const errors: Array<{path: string; message : string}> = [];
    err.inner.forEach (e => {
        errors.push({
            path:e.path,
            message: e.message
        })
        
    })
    return errors;
}

export const resolvers = {
    Query: {
      resolveUser: async (_: any, { email }: GQL.IResolveUserOnQueryArguments) => {
        const userTmp: Array<{id : string, name: string, email: string, loggedIn: boolean }> = [];
            const user = await User.findOne({where: {email}, select: ["name", "email", "loggedIn"]});
            userTmp.push({id: '', name: user!.name, email: user!.email , 
                loggedIn: user!.loggedIn});
            return userTmp[0]; 
      },

      allUsers: async (_: any, { }: GQL.IResolveUserOnQueryArguments) => {
         const userTmp: Array<{id : string, name: string, email: string, loggedIn: boolean  }> = [];

        await User.find({}).then(user => {
            user.forEach( i =>{
                userTmp.push({id: i.id, name: i.name, email: i.email, 
                    loggedIn: i.loggedIn });
            });
           
        });
        return userTmp;
     },

     allPosts: async (_: any, { owner }: GQL.IAllPostsOnQueryArguments) => {
        const postTmp: Array<{description : string, content: string, owner:{id : string, name: string, 
            email: string, loggedIn: boolean } }> = [];

        await Post.find({relations: ['user']}).then(posts => {
           posts.forEach( i =>{

               if(i.user.id === owner || owner==null ){

               postTmp.push({
                   owner: {id: i.user.id, name: i.user.name, email: i.user.email, 
                    loggedIn: i.user.loggedIn },

                   description : i.description,
                   content: i.content
               });
            };
            
           });
          
       });
       return postTmp;
    }},

    Mutation: {
        createUser: async (_: any, args: GQL.ICreateUserOnMutationArguments) => {
            try{
                await schemaUserAccount.validate(args, {abortEarly: false});
            } catch(err){
                return formatErrors(err);
            }

            const {name,email,password} = args;
            const passwordHash = await bcrypt.hash(password, 10);
            const userTmp = await User.findOne({email});
            if(userTmp){
                return [
                    {
                        path:"email",
                        message: "duplicate email"
                    }
                ]
            }

            const user = User.create({
                name,
                email,
                password: passwordHash,
                loggedIn: false,
            });
            await user.save();
            return [{payload: user.id}];
        },

        updateUser:  async (_: any, args : GQL.IUpdateUserOnMutationArguments) => {

            try{
                await schemaUserAccount.validate(args, {abortEarly: false});
            } catch(err){
                return formatErrors(err);
            }

            const {id, name, email} = args;
            const user = await User.findOne({where: {id}});

                if(user!=null && user.loggedIn) {

                if(name!=null)
                    user.name = name;
                if(email!=null)
                    user.email = email;
                   user.save();
               }
               else{
                return [
                    {
                        path:"authentication",
                        message: "authentication error"
                    }
                ]
               }
            
            return null;
    },

    loginUser:  async (_: any, args : GQL.ILoginUserOnMutationArguments) => {

        try{
            await schemaUserAccount.validate(args, {abortEarly: false});
        } catch(err){
            return formatErrors(err);
        }

        const loginError = [{
            path: "login",
            message: "invalid login"
        }];
        const {email, password} = args;
        const user = await User.findOne({where: {email}});

        if(user == null){
            return loginError;
        }

        const validate = await bcrypt.compare(password, user.password);

        if(validate == null){
            return loginError;
        }

        user.loggedIn = true;
        user.save();
        
        return [{payload: user.id}];
},

logoutUser:  async (_: any, args : GQL.ILogoutUserOnMutationArguments) => {

    const {id} = args;
    const user = await User.findOne({where: {id}});

    if(user!=null){
    user.loggedIn = false;
    user.save();
    }
    else{
        return [
            {
                path:"user",
                message: "invalid user"
            }
        ]
    }
    
    return null;;
},
    createPost:  async (_: any, args: GQL.ICreatePostOnMutationArguments) => {
        try{
            await schemaCreatePost.validate(args, {abortEarly: false});
        } catch(err){
            return formatErrors(err);
        }
        const {owner, description, content} = args; 
        const errors: Array<{path: string; message : string}> = [];
        const user = await User.findOne({where: {id: owner}});
            
            if(user !=null) {
                    let post = new Post();
                    post.content = content;
                    post.description = description;
                    post.user = user;
                    post.user.save();
                    post.save();
           }
           else{

            errors.push({
                    path:"id",
                    message: "user id does not exist"
            });
                
            }

        return errors;
}}
  };