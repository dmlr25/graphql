// tslint:disable
// graphql typescript definitions

export declare namespace GQL {
    interface IGraphQLResponseRoot {
    data?: IQuery | IMutation;
    errors?: Array<IGraphQLResponseError>;
    }
    
    interface IGraphQLResponseError {
    /** Required for all errors */
    message: string;
    locations?: Array<IGraphQLResponseErrorLocation>;
    /** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
    [propName: string]: any;
    }
    
    interface IGraphQLResponseErrorLocation {
    line: number;
    column: number;
    }
    
    interface IQuery {
    __typename: "Query";
    resolveUser: IUser;
    allUsers: Array<IUser | null> | null;
    allPosts: Array<IPost | null> | null;
    }
    
    interface IResolveUserOnQueryArguments {
    email: string;
    }
    
    interface IAllPostsOnQueryArguments {
    owner?: string | null;
    }
    
    interface IUser {
    __typename: "User";
    id: string;
    name: string;
    email: string;
    loggedIn: boolean;
    }
    
    interface IPost {
    __typename: "Post";
    description: string | null;
    content: string | null;
    owner: IUser | null;
    }
    
    interface IMutation {
    __typename: "Mutation";
    createUser: Array<IError> | null;
    loginUser: Array<IError> | null;
    logoutUser: Array<IError> | null;
    updateUser: Array<IError> | null;
    createPost: Array<IError> | null;
    }
    
    interface ICreateUserOnMutationArguments {
    name: string;
    email: string;
    password: string;
    }
    
    interface ILoginUserOnMutationArguments {
    email: string;
    password: string;
    }
    
    interface ILogoutUserOnMutationArguments {
    id: string;
    }
    
    interface IUpdateUserOnMutationArguments {
    id: string;
    name?: string | null;
    email?: string | null;
    }
    
    interface ICreatePostOnMutationArguments {
    owner: string;
    description: string;
    content: string;
    }
    
    interface IError {
    __typename: "Error";
    path: string | null;
    message: string | null;
    payload: string | null;
    }
    }
    
    // tslint:enable
    