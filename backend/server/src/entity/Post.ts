import {Entity, PrimaryColumn, Column, BeforeInsert, BaseEntity, ManyToOne} from "typeorm";
import {User} from './User';

import * as uuidv4 from "uuid/v4";

@Entity("posts")
export class Post extends BaseEntity {

    // uuid simulates private key
    @PrimaryColumn("uuid")
    id: string;

    // Limit name
    @Column("varchar", {length:255})
    description: string;
    
    // Limit email
    @Column("varchar", {length:255})
    content: string;

    // User may be associated with many posts
    // define many to one relation
    @ManyToOne(()=>User, (user)=> user.posts)
    public user: User;

    @BeforeInsert()
    addId(){
        this.id = uuidv4();    
    }

}
