import {Entity, PrimaryColumn, Column, BeforeInsert, BaseEntity, OneToMany} from "typeorm";
import {Post} from './Post';
import * as uuidv4 from "uuid/v4";

@Entity("users")
export class User extends BaseEntity {

    @PrimaryColumn("uuid")
    id: string;
   
    @Column("varchar", {length:255})
    name: string;
    
    @Column("varchar", {length:255})
    email: string;

    @Column("text")
    password: string;

    @Column("bool")
    loggedIn: boolean;

    @OneToMany(()=> Post, (post)=>post.user )
    public posts: Post[];

    @BeforeInsert()
    addId(){
        this.id = uuidv4();    
    }

}
