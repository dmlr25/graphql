import { Component, OnInit, Input } from '@angular/core';
import {Post, User} from '../types';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {

  constructor() { }

  @Input()
  user: User;

  ngOnInit() {
  }

}
