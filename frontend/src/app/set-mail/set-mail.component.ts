import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {RESOLVE_USER_QUERY, UPDATE_USER_MUTATION} from '../graphql';
import { stringify } from '@angular/core/src/render3/util';
import {LOGIN_USER_MUTATION, LOGOUT_USER_MUTATION} from '../graphql';
import {AppComponent} from '../app.component';



@Component({
  selector: 'app-set-mail',
  templateUrl: './set-mail.component.html',
  styleUrls: ['./set-mail.component.css']
})
export class SetMailComponent implements OnInit {
  @ViewChild('username') username: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('email') email: ElementRef;

  constructor(private apollo: Apollo) { }

  ngOnInit() {
  }


  update() {

    const user = this.username.nativeElement.value;
    const em = this.email.nativeElement.value;

    if (AppComponent.uuid == null) {
      window.alert('User not signed in!');
    } else {
    this.apollo.mutate({
      mutation: UPDATE_USER_MUTATION,
      variables: {
        id: AppComponent.uuid,
        email: em,
        name: user
      }
    }).subscribe((respons) => {
        window.alert('Credientials Updated');
    });
  }
}

}
