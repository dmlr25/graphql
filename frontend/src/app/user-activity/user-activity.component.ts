import { ElementRef,  Component, OnInit, ViewChild } from '@angular/core';
import { Apollo } from 'apollo-angular';
import {CREATE_POST_MUTATION, RESOLVE_USER_QUERY} from '../graphql';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-user-activity',
  templateUrl: './user-activity.component.html',
  styleUrls: ['./user-activity.component.css']
})
export class UserActivityComponent implements OnInit {

  constructor(private apollo: Apollo) { }

  @ViewChild('description') description: ElementRef;
  @ViewChild('message') message: ElementRef;

  post() {
    const description: string = this.description.nativeElement.value;
    const message: string = this.message.nativeElement.value;

    this.apollo.mutate({
      mutation: CREATE_POST_MUTATION,
      variables: {
        id: AppComponent.uuid,
        ct: message,
        desc: description
      }
    }).subscribe((respons) => {
        window.alert('Message Posted');
    });
  }

  ngOnInit() {
  }

}
