import { Component, OnInit, Input } from '@angular/core';
import {Apollo} from 'apollo-angular';
import {Post, User} from '../types';

import {ALL_USERS_QUERY} from '../graphql';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {

  @Input()
  post: Post;

  constructor(private apollo: Apollo) { }

  ngOnInit() {
  }

}
