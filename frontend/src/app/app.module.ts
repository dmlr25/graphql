import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {GraphQLModule} from './apollo.config';
import { PostComponent } from './post/post.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { UserViewComponent } from './user-view/user-view.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserActivityComponent } from './user-activity/user-activity.component';
import { SetMailComponent } from './set-mail/set-mail.component';


@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    UserPostsComponent,
    UserViewComponent,
    UserRegisterComponent,
    UserLoginComponent,
    UserActivityComponent,
    SetMailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphQLModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
