import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Apollo} from 'apollo-angular';
import {REGISTER_USER_MUTATION} from '../graphql';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  @ViewChild('username') username: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('email') email: ElementRef;

  constructor( private apollo: Apollo) { }

  ngOnInit() {
  }

  register() {

    const user = this.username.nativeElement.value;
    const ps = this.password.nativeElement.value;
    const em = this.email.nativeElement.value;

    this.apollo.mutate({
      mutation: REGISTER_USER_MUTATION,
      variables: {
        name: user,
        password: ps,
        email: em
      }
    }).subscribe((response) => {

      response.data.createUser.forEach(element => {
        if (element.message == null) {
          window.alert('User registered, please refresh the page');
        } else {
        window.alert(element.message);
        }
      });

    });
  }

}
