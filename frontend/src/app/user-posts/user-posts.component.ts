import { Component, OnInit } from '@angular/core';
import {Post, User} from '../types';
import {Apollo} from 'apollo-angular';
import {ALL_USERS_QUERY, ALL_POSTS_QUERY} from '../graphql';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {

  allPosts: Post[] = [];
  allUsers: User[] = [];

  constructor(private apollo: Apollo) { }

  ngOnInit() {
    this.apollo.query({query: ALL_POSTS_QUERY}).subscribe(
      (response) => {
        /* tslint:disable:no-string-literal */
        this.allPosts = response.data['allPosts'];
        console.log(response.data['allPosts']);
      }

    );

    this.apollo.query({query: ALL_USERS_QUERY}).subscribe(
      (response) => {
        /* tslint:disable:no-string-literal */
        this.allUsers = response.data['allUsers'];
        console.log('fetched all users');
      }

    );
  }

}
