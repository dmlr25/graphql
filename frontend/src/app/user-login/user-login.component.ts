import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {LOGIN_USER_MUTATION, LOGOUT_USER_MUTATION} from '../graphql';
import {AppComponent} from '../app.component';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {


  @ViewChild('email') email: ElementRef;
  @ViewChild('password') password: ElementRef;

  constructor(private apollo: Apollo) { }

  ngOnInit() {
  }


    login() {

    const ps = this.password.nativeElement.value;
    const em = this.email.nativeElement.value;

    this.apollo.mutate({
      mutation: LOGIN_USER_MUTATION,
      variables: {
        password: ps,
        email: em
      }
    }).subscribe((response) => {
      if (AppComponent.uuid == null) {
      response.data.loginUser.forEach(element => {
        if (element.payload == null) {
          window.alert('Invalid credentials');
        } else {
        AppComponent.uuid = element.payload;
        window.alert('User logged in with UUID: ' + element.payload);
        }
      });
    } else {
      window.alert('User already logged in!');
    }

    });
  }

  logout() {

    AppComponent.uuid = null;
  }

}
