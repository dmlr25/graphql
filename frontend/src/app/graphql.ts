import {Post, User} from './types';
import gql from 'graphql-tag';

export const ALL_USERS_QUERY = gql`
    query AllUsersQuery{
            allUsers{
            id
            name
            email
            }
    }
`;

export const ALL_POSTS_QUERY = gql`
    query AllPostsQuery{
        allPosts{
            description
            content
            owner{
                name
            }
    }}
`;
export const LOGIN_USER_MUTATION = gql`
    mutation LoginUserMutation( $email : String!, $password: String! ){
    loginUser(
            password: $password,
            email: $email
              ){
                   payload
            }
    }
`;

export const LOGOUT_USER_MUTATION = gql`
    mutation LogoutUserMutation( $id : String! ){
    logoutUser(
            id: $id
              ){
                  path
                  message
              }
    }
`;

export const REGISTER_USER_MUTATION = gql`
    mutation RegisterUserMutation($name : String!, $password: String!, $email : String!){
    createUser(
            name : $name,
            password: $password,
            email: $email
              ){
                   path
                   message
            }
    }
`;


export const CREATE_POST_MUTATION = gql`
    mutation CreatePostMutation($id : ID!, $desc: String!, $ct : String! ){
        createPost(
                owner : $id,
                description: $desc,
                content: $ct
                ){
                    path
                    message
                }
    }
`;


export const UPDATE_USER_MUTATION = gql`
    mutation UpdateUserMutation($id : ID!, $name: String!, $email : String!){
        updateUser(
                id : $id,
                name: $name,
                email: $email){
                     path
                     message
                }

    }
`;

export const RESOLVE_USER_QUERY = gql`
    query ResolveUserQuery($email : String!){
            resolveUser(email : $email){
                    name
            }
        }
`;
