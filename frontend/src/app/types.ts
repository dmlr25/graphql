export class Post {
    description: string;
    content: string;
    owner: User;
}

export class User {
    id: string;
    name: string;
    email: string;
    loggedIn: boolean;
  }

